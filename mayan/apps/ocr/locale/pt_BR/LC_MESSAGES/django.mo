��          �   %   �      0     1  4   7     l     t     }     �     �     �     �     �     �  2   �  M   1  .        �  
   �     �     �     �     �  	             $  �  6  
   �  4   �  	     
        *  	   @      J     k     �     �     �  6   �  S   �  <   R     �     �     �  	   �     �     �          !     7                         	                                                                                              
        Added Automatically queue newly created documents for OCR. Content Contents Date time submitted Document Document Version OCR Error Document Version OCR Errors Document page Document type Document version Document: %(document)s was added to the OCR queue. File path to poppler's pdftotext program used to extract text from PDF files. Full path to the backend to be used to do OCR. OCR OCR errors Page %(page_number)d Result Submit documents for OCR error getting version not found pdftotext version tesseract version Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:14-0400
PO-Revision-Date: 2016-03-21 21:11+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 adicionado Automatically queue newly created documents for OCR. Conteúdo Conteúdos Data e Hora Submetida Documento OCR erro: Versão do documento   OCR erros: Versão do documento página do documento Tipo de Documento Versão do Documento Documento: %(document)s foi adicionado à fila de OCR. Caminho para o programa poppler pdftotext usado para extrair texto de arquivos PDF. Caminho completo para o servidor a ser usado para fazer OCR. Enviar para a  fila de OCR Erros de OCR Pagina: %(page_number)d resultado Submeter documentos para OCR Erro ao receber a versão. Não encontrada Versão do  pdftotext Versão do tesseract 